export type RecognitionResult = {
  id: Number;
  imageSrc: string;
  size: Number;
  name: string;
  result: Number;
};
