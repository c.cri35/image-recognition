export type SchdeduledEvent = {
  id: Number;
  startDate: Date;
  endDate: Date;
};
