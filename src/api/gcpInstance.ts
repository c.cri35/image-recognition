import axios from "axios";

const instance = axios.create({
  baseURL: process.env.REACT_APP_GCP_BASE_URL,
  headers: { "content-type": "application/json;charset=UTF-8" },
});

export default instance;
