import axios, { AxiosRequestConfig } from "axios";

const instance = axios.create({
  baseURL: process.env.REACT_APP_BASE_URL,
  headers: { "content-type": "application/json;charset=UTF-8" },
});

instance.interceptors.request.use(
  function (config: AxiosRequestConfig) {
    const token = localStorage.getItem("token");
    if (token && config.headers) {
      config.headers.Authorization = `Bearer ${token}`;
    }

    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

instance.interceptors.response.use(
  (res) => res,
  function (error) {
    console.log(error.response);

    if (error.response.status === 401) {
      alert("Nu aveti acces la metoda: " + error.response.config.url);
    }
    return Promise.reject(error);
  }
);

export default instance;
