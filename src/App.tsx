import React from "react";
import "./App.scss";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import LoginPage from "./pages/LoginPage";
import TablePage from "./pages/TablePage";
import UploadPage from "./pages/UploadPage";
import DatePickerPage from "./pages/SchedulerPage";
import { UserDataContext } from "./UserDataContext";
import useLocalStorage from "./hooks/useLocalStorage";

const App = () => {
  const [isLoggedIn] = useLocalStorage("isLoggedIn", "false");
  return (
    <UserDataContext.Provider value={isLoggedIn === "true"}>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" render={() => LoginPage()} />
          <Route exact path="/table" render={() => TablePage()} />
          <Route exact path="/upload" render={() => <UploadPage />} />
          <Route exact path="/scheduler" render={() => <DatePickerPage />} />
        </Switch>
      </BrowserRouter>
    </UserDataContext.Provider>
  );
};

export default App;
