import React, { ChangeEvent, useRef, useState } from "react";
import RedirectPage from "../components/RedirectPage";

import {
  Button,
  Card,
  CardHeader,
  CardMedia,
  Snackbar,
  Alert,
} from "@mui/material";
import { Upload, Save } from "@mui/icons-material";
import MainTable from "../components/MainTable";
import axios from "../api/instance";
import useLocalStorage from "../hooks/useLocalStorage";

const ACCEPTED_TYPE = "image/png";
const ACCEPTED_SIZE = 28;

const UploadPage: React.FC<{}> = ({ children }) => {
  const [imageSrc, setImageSrc] = useState(
    "https://natureconservancy-h.assetsadobe.com/is/image/content/dam/tnc/nature/en/photos/Zugpsitze_mountain.jpg?crop=0,176,3008,1654&wid=4000&hei=2200&scl=0.752"
  );
  const [width, setWidth] = useState<number>();
  const [height, setHeight] = useState<number>();

  const [file, setFile] = useState<File>();
  const [userId] = useLocalStorage("userId", "");

  const [showSnackbar, setShowSnackBar] = useState(false);

  const inputFile = useRef<HTMLInputElement | null>(null);
  const openFileDialog = () => {
    if (inputFile.current != null) {
      inputFile.current.click();
    }
  };

  const onSaveFile = (event: ChangeEvent<HTMLInputElement>) => {
    event.stopPropagation();
    event.preventDefault();
    if (event != null && event.target != null) {
      let file = (event.target as HTMLInputElement).files!.item(0);

      if (file != null) {
        setFile(file);

        var reader = new FileReader();

        reader.onloadend = function () {
          var img = new Image();
          img.onload = function () {
            setHeight(img.height);
            setWidth(img.width);
          };

          if (reader.result != null) {
            img.src = reader.result.toString();
            setImageSrc(reader.result.toString());
          }
        };

        reader.readAsDataURL(file);
      }
    }
  };

  const saveImage = () => {
    var formData = new FormData();
    if (file) {
      formData.append("image", file);
      formData.append("name", file.name);
      formData.append("size", file.size.toString());
      formData.append("userId", userId);
      axios
        .post("evaluate", formData, {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        })
        .then((response) => {
          if (response.status === 200) {
            setShowSnackBar(true);
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  const handleCloseSnackbar = (e: React.SyntheticEvent | Event) => {
    setShowSnackBar(false);
  };

  const uploadAllowed = () => {
    if (!file) return false;
    if (file.type !== ACCEPTED_TYPE) return false;
    if (height !== width) return false;
    if (height !== ACCEPTED_SIZE) return false;

    return true;
  };

  return (
    <RedirectPage path="/" redirectIfLogged={false}>
      <MainTable
        showLogoutButton={true}
        backgroundColor="#ffffff"
        contentPadding="50px 200px"
      >
        <Card style={{ borderRadius: "25px", height: "80vh" }} elevation={16}>
          <CardHeader
            action={
              <div style={{ padding: "5px 10px" }}>
                <Button
                  variant="contained"
                  startIcon={<Upload />}
                  onClick={openFileDialog}
                >
                  Upload
                </Button>
                <Button
                  variant="contained"
                  startIcon={<Save />}
                  disabled={!uploadAllowed()}
                  onClick={saveImage}
                  style={{ marginLeft: "10px" }}
                >
                  Save
                </Button>
              </div>
            }
          />
          <CardMedia style={{ textAlign: "center" }}>
            <img
              style={{
                width: "auto",
                height: "auto",
                maxHeight: "100%",
                maxWidth: "100%",
              }}
              alt="img"
              src={imageSrc}
            />
          </CardMedia>
        </Card>
        <input
          type="file"
          id="file"
          accept={ACCEPTED_TYPE}
          ref={inputFile}
          style={{ display: "none" }}
          onChange={onSaveFile.bind(this)}
        />
        <Snackbar
          open={showSnackbar}
          onClose={handleCloseSnackbar}
          autoHideDuration={4000}
        >
          <Alert
            onClose={handleCloseSnackbar}
            severity="success"
            sx={{ width: "100%" }}
          >
            Image uploaded successfully
          </Alert>
        </Snackbar>
      </MainTable>
    </RedirectPage>
  );
};

export default UploadPage;
