import React from "react";
import RedirectPage from "../components/RedirectPage";
import TableForm from "../components/TableForm";
import MainTable from "../components/MainTable";

const TablePage = () => {
  return (
    <RedirectPage path="/" redirectIfLogged={false}>
      <MainTable
        showLogoutButton={true}
        backgroundColor="#cccccc"
        contentPadding="50px"
      >
        <TableForm />
      </MainTable>
    </RedirectPage>
  );
};

export default TablePage;
