import {
  Button,
  CircularProgress,
  Snackbar,
  Stack,
  Alert,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import DatePicker from "../components/DatePicker";
import MainTable from "../components/MainTable";
import SchedulerForm from "../components/SchedulerForm";
import axios from "../api/gcpInstance";
import { SchdeduledEvent } from "../types/ScheduledEvent";
import moment from "moment";

const DatePickerPage = () => {
  const [placeholderId, setPlaceholderId] = useState(0);

  const [startDate, setStartDate] = useState<Date | null>(null);
  const [endDate, setEndDate] = useState<Date | null>(null);

  const [loaded, setLoaded] = useState(false);
  const [rows, setRows] = useState<Array<SchdeduledEvent>>([]);

  const [showSnackbar, setShowSnackBar] = useState(false);

  useEffect(() => {
    axios
      .get("getScheduledTasks")
      .then((response) => {
        if (response.status === 200) {
          let scheduledEvents = (
            response.data as Array<{ start_date: Date; end_date: Date }>
          ).map(
            (element, index) =>
              ({
                id: index,
                startDate: element.start_date,
                endDate: element.end_date,
              } as SchdeduledEvent)
          );
          setRows(
            scheduledEvents.sort((a, b) =>
              moment(a.startDate).isBefore(moment(b.startDate)) ? -1 : 1
            )
          );
          setLoaded(true);
          setPlaceholderId(scheduledEvents.length);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const handleErrorOnAdd = (task: SchdeduledEvent) => {
    setRows(rows.filter((item) => item.id !== task.id));
    setShowSnackBar(true);
  };

  const addTask = () => {
    let task: SchdeduledEvent = {
      id: placeholderId,
      startDate: startDate!,
      endDate: endDate!,
    };

    //optimisic update
    setPlaceholderId(placeholderId + 1);
    setRows([...rows, task]);

    //clear form
    setStartDate(null);
    setEndDate(null);

    axios
      .post("addScheduledTask", {
        start_date: startDate,
        end_date: endDate,
      })
      .then((response) => {
        if (response.status !== 200) {
          handleErrorOnAdd(task);
        }
      })
      .catch((e) => {
        handleErrorOnAdd(task);
      });
  };

  const handleCloseSnackbar = (e: React.SyntheticEvent | Event) => {
    setShowSnackBar(false);
  };

  if (!loaded)
    return (
      <div
        style={{
          margin: "auto",
          textAlign: "center",
          width: "100%",
          background: "#cccccc",
          height: "100vh",
        }}
      >
        <CircularProgress />
      </div>
    );

  return (
    <MainTable
      showLogoutButton={false}
      backgroundColor="#cccccc"
      contentPadding="50px"
    >
      <Stack direction="row" alignItems="center">
        <DatePicker
          start={startDate}
          end={endDate}
          onStartChanged={(date: Date | null) => setStartDate(date)}
          onEndChanged={(date: Date | null) => setEndDate(date)}
        />
        <Button
          variant="contained"
          style={{ marginLeft: "10px" }}
          onClick={addTask}
          disabled={!startDate || !endDate}
        >
          ADD TASK
        </Button>
      </Stack>
      <SchedulerForm scheduledTasks={rows} />
      <Snackbar
        open={showSnackbar}
        onClose={handleCloseSnackbar}
        autoHideDuration={4000}
      >
        <Alert
          onClose={handleCloseSnackbar}
          severity="error"
          sx={{ width: "100%" }}
        >
          An error occured when adding the task.
        </Alert>
      </Snackbar>
    </MainTable>
  );
};

export default DatePickerPage;
