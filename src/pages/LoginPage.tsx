import React from "react";

import { Card, CardContent, Avatar, Stack, Paper } from "@mui/material";

import AvatarImage from "../avatar.png";
import LoginForm from "../components/LoginForm";
import RedirectPage from "../components/RedirectPage";
import MainTable from "../components/MainTable";

const LoginPage = () => {
  return (
    <RedirectPage path="/table" redirectIfLogged={true}>
      <MainTable
        showLogoutButton={false}
        backgroundColor="linear-gradient(to right top, #4056c7, #c950c1)"
        contentPadding="60px"
      >
        <Card
          style={{
            height: "80vh",
            textAlign: "center",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            borderRadius: "25px",
          }}
        >
          <CardContent>
            <Stack
              direction="row"
              justifyContent="space-around"
              alignItems="center"
            >
              <Avatar
                component={Paper}
                elevation={15}
                src={AvatarImage}
                style={{ padding: "45px" }}
                sx={{ width: "150px", height: "150px" }}
              />
              <LoginForm />
            </Stack>
          </CardContent>
        </Card>
      </MainTable>
    </RedirectPage>
  );
};

export default LoginPage;
