import React from "react";
import { Redirect } from "react-router";
import { UserDataContext } from "../UserDataContext";

type RedirectProps = {
  path: string;
  redirectIfLogged: boolean;
};

const RedirectPage: React.FC<RedirectProps> = ({
  children,
  path,
  redirectIfLogged,
}) => {
  return (
    <UserDataContext.Consumer>
      {(isLoggedIn) => (
        <div>
          {children}
          {(!isLoggedIn && redirectIfLogged) ||
          (isLoggedIn && !redirectIfLogged) ? null : (
            <Redirect to={path} />
          )}
        </div>
      )}
    </UserDataContext.Consumer>
  );
};

export default RedirectPage;
