import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import React from "react";
import LogoutButton from "./LogoutButton";

type TableProps = {
  showLogoutButton: boolean;
  backgroundColor: string;
  contentPadding: string;
};

const MainTable: React.FC<TableProps> = ({
  children,
  showLogoutButton,
  backgroundColor,
  contentPadding,
}) => {
  return (
    <TableContainer
      style={{
        background: backgroundColor,
      }}
    >
      <Table
        style={{
          height: "100vh",
        }}
      >
        {showLogoutButton ? (
          <TableHead>
            <LogoutButton />
          </TableHead>
        ) : null}
        <TableBody>
          <TableRow>
            <TableCell style={{ padding: contentPadding }}>
              {children}
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default MainTable;
