import { Button } from "@mui/material";
import React from "react";

const CalendarButton = (props: any) => (
  <Button
    variant="contained"
    onClick={props.onClick}
    style={{
      borderRadius: props.borderRadius,
      backgroundColor: props.backgroundColor,
      width: props.width,
      color: props.color,
    }}
  >
    {props.text}
  </Button>
);

export default CalendarButton;
