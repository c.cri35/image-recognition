import React, { useState, useEffect } from "react";
import CalendarHeader from "./CalendarHeader";
import CalendarContent from "./CalendarContent";
import CalendarActions from "./CalendarActions";
import { Divider, Stack } from "@mui/material";
import moment from "moment";
import "./CalendarContent.scss";

type CalendarProps = {
  start: Date | null;
  end: Date | null;
  onChanged: (date: Date | null) => void;
  onDone: () => void;
  onCancel: () => void;
};

const Calendar: React.FC<CalendarProps> = ({
  start,
  end,
  onChanged,
  onDone,
  onCancel,
}) => {
  const [dateInfo, setDateInfo] = useState({
    days: Array<Date>(),
    month: moment().month(),
  });

  const getLastDateAndDaysNumberOfMonth = (month: number, year: number) => {
    var date = new Date(year, month, 0);

    return [date.getDay() + 1, date.getDate()];
  };

  const onMonthOrYearChange = (month: number, year: number) => {
    const [lastDayOfLastMonthIndex, daysInLastMonth] =
      getLastDateAndDaysNumberOfMonth(month, year);
    const [lastDayOfMonthIndex, daysInMonth] = getLastDateAndDaysNumberOfMonth(
      month + 1,
      year
    );

    let days = [...Array(daysInMonth)].map(
      (_, i) => new Date(year, month, 1 + i)
    );
    let prefixDays: Array<Date> = [];
    let sufixDays: Array<Date> = [];

    if (lastDayOfLastMonthIndex !== 7)
      prefixDays = [...Array(lastDayOfLastMonthIndex)].map(
        (_, i) =>
          new Date(
            year,
            month - 1,
            daysInLastMonth - lastDayOfLastMonthIndex + 1 + i
          )
      );

    sufixDays = [...Array(7 - lastDayOfMonthIndex)].map(
      (_, i) => new Date(year, month + 1, 1 + i)
    );
    setDateInfo({
      days: prefixDays.concat(days).concat(sufixDays),
      month: month,
    });
  };

  useEffect(() => {
    onMonthOrYearChange(
      moment().month(),
      Number.parseInt(moment().format("YYYY"))
    );
  }, []);

  return (
    <Stack
      direction="column"
      justifyContent="space-around"
      style={{
        width: "300px",
        position: "absolute",
        zIndex: 1,
        background: "linear-gradient(to left, #4056c7, #7f20c7)",
        borderRadius: "0px 0px 12px 12px",
      }}
      divider={<Divider orientation="horizontal" flexItem color="#4056c7" />}
    >
      <CalendarHeader onMonthOrYearChange={onMonthOrYearChange} />
      <CalendarContent
        dateInfo={dateInfo}
        start={start}
        end={end}
        onChanged={onChanged}
      />
      <CalendarActions onDone={onDone} onCancel={onCancel} />
    </Stack>
  );
};

export default Calendar;
