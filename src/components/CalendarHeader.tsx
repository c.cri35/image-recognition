import React, { useState } from "react";
import { Stack, MenuItem, SelectChangeEvent } from "@mui/material";
import Divider from "@mui/material/Divider";
import moment from "moment";
import CustomSelect from "./CustomSelect";

const CalendarHeader = (props: any) => {
  const [displayedMonthIndex, setDisplayedMonth] = useState(moment().month());
  const [displayedYear, setDisplayedYear] = useState(
    Number.parseInt(moment().format("YYYY"))
  );
  return (
    <Stack
      direction="row"
      justifyContent="space-around"
      alignItems="center"
      divider={<Divider orientation="vertical" flexItem color="white" />}
      style={{
        boxShadow: "0 8px 6px -6px rgba(0, 0, 0, 0.5)",
        cursor: "pointer",
      }}
    >
      <p
        style={{
          color: "white",
          textAlign: "center",
          marginTop: 10,
          fontWeight: "bold",
        }}
      >
        <span style={{ fontSize: "20px" }}>{moment().format("DD")}</span>
        <br />
        TODAY
      </p>
      <CustomSelect
        value={displayedMonthIndex.toString()}
        onChange={(e: SelectChangeEvent) => {
          setDisplayedMonth(Number.parseInt(e.target.value));
          props.onMonthOrYearChange(e.target.value, displayedYear);
        }}
      >
        {moment.months().map((month, index) => (
          <MenuItem value={index}>{month.toUpperCase()}</MenuItem>
        ))}
      </CustomSelect>
      <CustomSelect
        value={displayedYear.toString()}
        onChange={(e: SelectChangeEvent) => {
          setDisplayedYear(Number.parseInt(e.target.value));
          props.onMonthOrYearChange(displayedMonthIndex, e.target.value);
        }}
      >
        {[...Array(150)]
          .map((_, i) => 1900 + i * 1)
          .map((year: number) => (
            <MenuItem value={year}>{year.toString()}</MenuItem>
          ))}
      </CustomSelect>
    </Stack>
  );
};

export default CalendarHeader;
