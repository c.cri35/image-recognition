import { Stack } from "@mui/material";
import React from "react";
import CalendarButton from "./CalendarButton";

type CalendarActionsProps = {
  onDone: () => void;
  onCancel: () => void;
};

const CalendarActions: React.FC<CalendarActionsProps> = ({
  onDone,
  onCancel,
}) => {
  return (
    <Stack
      direction="row"
      justifyContent="space-around"
      style={{
        padding: "10px",
        boxShadow: "0px -2px 4px rgba(0, 0, 0, 0.5)",
        borderRadius: "0px 0px 12px 12px",
      }}
    >
      <CalendarButton
        borderRadius="16px"
        backgroundColor="#fcfcfc"
        color="grey"
        width="100px"
        text="CANCEL"
        onClick={onCancel}
      />
      <CalendarButton
        borderRadius="16px"
        backgroundColor="#eb71ce"
        color="white"
        width="100px"
        text="DONE"
        onClick={onDone}
      />
    </Stack>
  );
};

export default CalendarActions;
