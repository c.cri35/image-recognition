import React from "react";

const CalendarCell = (props: any) => (
  <div
    style={{
      fontWeight: "bold",
      fontSize: "12px",
      padding: "8px 0px",
      margin: "8px 0px",
      textAlign: "center",
      color: "white",
      ...(props.isFromAnotherMonth && {
        color: "rgba(255, 255, 255, 0.5)",
      }),
      ...(props.currentDay && {
        backgroundColor: "#05299E",
      }),
      ...(props.currentDay &&
        !props.isPartOfSelection && {
          borderRadius: "20px",
        }),
      ...(props.isPartOfSelection && {
        backgroundColor: "#00b0ff",
      }),
      ...(props.start && {
        borderRadius: "20px 0px 0px 20px",
      }),
      ...(props.end && {
        borderRadius: "0px 20px 20px 0px",
      }),
      ...(props.start &&
        props.end && {
          borderRadius: "20px",
        }),
    }}
    onClick={props.onClick}
  >
    {props.text}
  </div>
);

export default CalendarCell;
