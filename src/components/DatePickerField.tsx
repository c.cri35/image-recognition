import React from "react";
import moment from "moment";

type DatePickerFieldProps = {
  label: string;
  date: Date | null;
  color: string;
};

const DatePickerField: React.FC<DatePickerFieldProps> = (
  props: DatePickerFieldProps
) => {
  return (
    <div>
      <p>{props.label} Date</p>{" "}
      <p style={{ color: props.color, fontWeight: "bold", fontSize: "15px" }}>
        {props.date
          ? moment(props.date).format("DD MMM YYYY").toUpperCase()
          : "DD/MM/YYYY"}
      </p>
    </div>
  );
};

export default DatePickerField;
