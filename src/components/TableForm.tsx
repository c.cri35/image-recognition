import React, { useState, useEffect } from "react";
import axios from "../api/instance";

import { Button, CircularProgress } from "@mui/material";
import { DataGrid, GridColDef, GridRowData } from "@mui/x-data-grid";
import useLocalStorage from "../hooks/useLocalStorage";
import { RecognitionResult } from "../types/RecognitionResult";

const downloadImage = (row: GridRowData) => {
  let blob = new Blob([new Uint8Array(row.image.data)], { type: "image/png" });
  let link = document.createElement("a");

  link.href = URL.createObjectURL(blob);

  link.setAttribute("visibility", "hidden");
  link.download = row.name;

  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
};

const columns: GridColDef[] = [
  {
    field: "id",
    hide: true,
  },
  {
    field: "name",
    headerName: "IMAGE NAME",
    flex: 1,
    headerAlign: "center",
  },
  {
    field: "size",
    headerName: "SIZE (KB)",
    type: "number",
    flex: 1,
    headerAlign: "center",
  },
  {
    field: "result",
    headerName: "RECOGNITION RESULT",
    flex: 1,
    headerAlign: "center",
  },
  {
    field: "image",
    headerName: "IMAGE DOWNLOAD LINK",
    flex: 1,
    headerAlign: "center",
    renderCell: (params) => (
      <Button
        variant="contained"
        onClick={(e) => {
          e.stopPropagation();
          downloadImage(params.row);
        }}
      >
        Download
      </Button>
    ),
  },
];

const TableForm = () => {
  const [userId] = useLocalStorage("userId", "");

  const [loaded, setLoaded] = useState(false);
  const [rows, setRows] = useState<Array<RecognitionResult>>([]);

  useEffect(() => {
    axios
      .get(`getRecognitionResults/${userId}`)
      .then((response) => {
        if (response.status === 200) {
          setRows(response.data);
          setLoaded(true);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  return (
    <div style={{ height: "100%", width: "100%" }}>
      {loaded ? (
        <DataGrid
          style={{ backgroundColor: "#ffffff" }}
          rows={rows}
          columns={columns}
          pageSize={7}
          rowsPerPageOptions={[7]}
          checkboxSelection
        />
      ) : (
        <CircularProgress />
      )}
    </div>
  );
};

export default TableForm;
