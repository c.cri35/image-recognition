import React, { useState } from "react";
import DatePickerField from "../components/DatePickerField";
import { Stack, Divider } from "@mui/material";
import Calendar from "../components/Calendar";
import moment from "moment";

type DatePickerProps = {
  start: Date | null;
  end: Date | null;
  onStartChanged: (date: Date | null) => void;
  onEndChanged: (date: Date | null) => void;
};

const DatePicker: React.FC<DatePickerProps> = ({
  start,
  end,
  onStartChanged,
  onEndChanged,
}) => {
  const [isCalendarVisible, setIsCalendarVisible] = useState(false);

  const onDateChanged = (date: Date | null) => {
    if (!start) onStartChanged(date);
    else if (!end) {
      if (moment(date).isBefore(start)) {
        onStartChanged(date);
        return;
      }
      onEndChanged(date);
    } else {
      onStartChanged(date);
      onEndChanged(null);
    }
  };

  const onDone = () => {
    setIsCalendarVisible(false);
  };

  const onCancel = () => {
    setIsCalendarVisible(false);
  };

  return (
    <div
      style={{
        width: "300px",
      }}
    >
      <Stack
        direction="row"
        justifyContent="space-around"
        divider={
          <Divider orientation="vertical" style={{ margin: "16px" }} flexItem />
        }
        style={{
          background: "white",
          border: "1px solid LightGrey",
          borderRadius: "12px",
          marginBottom: "5px",
        }}
        onClick={(e) => setIsCalendarVisible(true)}
      >
        <DatePickerField label={"Start"} date={start} color={"#947be4"} />
        <DatePickerField label={"End"} date={end} color={"#72a5e7"} />
      </Stack>

      {isCalendarVisible ? (
        <Calendar
          start={start}
          end={end}
          onChanged={onDateChanged}
          onDone={onDone}
          onCancel={onCancel}
        />
      ) : null}
    </div>
  );
};

export default DatePicker;
