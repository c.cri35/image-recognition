import React, { useState } from "react";

import { Stack, TextField, Button, Link, Typography } from "@mui/material";

import { createTheme, ThemeProvider } from "@mui/material/styles";

import MailIcon from "@mui/icons-material/Mail";
import LockIcon from "@mui/icons-material/Lock";
import { useAuth } from "../hooks/useAuth";

const theme = createTheme({
  components: {
    MuiFilledInput: {
      styleOverrides: {
        root: {
          borderRadius: "25px",
        },
      },
    },
  },
});

const LoginForm = () => {
  const [, login] = useAuth();

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  return (
    <Stack spacing={2} sx={{ width: "25%" }}>
      <Typography variant="h3">User Login</Typography>
      <ThemeProvider theme={theme}>
        <TextField
          variant="filled"
          InputProps={{
            hiddenLabel: true,
            disableUnderline: true,
            startAdornment: <MailIcon />,
            placeholder: " Email Id",
          }}
          onChange={(e) => {
            setUsername(e.target.value);
          }}
        />
        <TextField
          type="password"
          variant="filled"
          InputProps={{
            hiddenLabel: true,
            disableUnderline: true,
            startAdornment: <LockIcon />,
            placeholder: " Password",
          }}
          onChange={(e) => {
            setPassword(e.target.value);
          }}
        />
      </ThemeProvider>
      <Button
        style={{ backgroundColor: "#56b847" }}
        variant="contained"
        sx={{ borderRadius: "25px" }}
        onClick={() => {
          login(username, password);
        }}
      >
        <div style={{ padding: "10px" }}>Login</div>
      </Button>
      <Link href="#" underline="hover">
        Forgot Username / Password?
      </Link>
    </Stack>
  );
};

export default LoginForm;
