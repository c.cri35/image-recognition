import React from "react";
import "./CalendarContent.scss";
import CalendarCell from "./CalendarCell";
import moment from "moment";

type CalendarContentProps = {
  dateInfo: { days: Array<Date>; month: number };
  start: Date | null;
  end: Date | null;
  onChanged: (date: Date | null) => void;
};

const CalendarContent: React.FC<CalendarContentProps> = ({
  dateInfo,
  start,
  end,
  onChanged,
}) => {
  const daysOfTheWeek: Array<String> = [
    "SU",
    "MO",
    "TU",
    "WE",
    "TH",
    "FR",
    "SA",
  ];

  const getHeader = () => {
    return (
      <div className="calendar-container">
        {daysOfTheWeek.map((day) => (
          <div className="header-cell">{day}</div>
        ))}
      </div>
    );
  };

  const onSelectDate = (date: Date) => {
    onChanged(date);
  };

  const getDays = () => {
    const calendar = [];

    for (let index = 0; index < dateInfo.days.length / 7; index++) {
      calendar.push(
        <div className="calendar-container">
          {[...Array(7)].map((_, i) => {
            let date = dateInfo.days[index * 7 + i];
            let dateMoment = moment(date);
            let current = moment();
            return (
              <CalendarCell
                isFromAnotherMonth={dateMoment.month() !== dateInfo.month}
                isPartOfSelection={
                  (start && !end && dateMoment.isSame(start)) ||
                  (dateMoment.isSameOrAfter(start) &&
                    dateMoment.isSameOrBefore(end))
                }
                currentDay={
                  dateMoment.year() === current.year() &&
                  dateMoment.month() === current.month() &&
                  date.getDate() === current.date()
                }
                start={dateMoment.isSame(start)}
                end={
                  (start && !end && dateMoment.isSame(start)) ||
                  dateMoment.isSame(end)
                }
                text={date.getDate()}
                onClick={() => onSelectDate(date)}
              />
            );
          })}
        </div>
      );
    }
    return <div style={{ padding: "16px 0px" }}>{calendar}</div>;
  };

  return (
    <div>
      {getHeader()}
      {getDays()}
    </div>
  );
};

export default CalendarContent;
