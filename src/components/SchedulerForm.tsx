import React from "react";

import { DataGrid, GridCellValue, GridColDef } from "@mui/x-data-grid";
import { SchdeduledEvent } from "../types/ScheduledEvent";
import moment from "moment";

const dateFormatter = (value: GridCellValue) =>
  moment(value!.toString()).format("DD MMM YYYY").toUpperCase();

const columns: GridColDef[] = [
  {
    field: "id",
    hide: true,
  },
  {
    field: "startDate",
    headerName: "Start Date",
    type: "date",
    flex: 1,
    valueFormatter: ({ value }) => dateFormatter(value),
    headerAlign: "center",
  },
  {
    field: "endDate",
    headerName: "End Date",
    type: "date",
    flex: 1,
    valueFormatter: ({ value }) => dateFormatter(value),
    headerAlign: "center",
  },
];

type SchedulerFormProps = {
  scheduledTasks: Array<SchdeduledEvent>;
};

const SchedulerForm: React.FC<SchedulerFormProps> = ({ scheduledTasks }) => {
  return (
    <div style={{ height: "100%", width: "100%" }}>
      <DataGrid
        style={{ backgroundColor: "#ffffff" }}
        rows={scheduledTasks}
        columns={columns}
        pageSize={7}
        rowsPerPageOptions={[7]}
        checkboxSelection
      />
    </div>
  );
};

export default SchedulerForm;
