import React from "react";
import { Select, FormControl } from "@mui/material";
import { makeStyles } from "@mui/styles";

const CustomSelect = (props: any) => {
  const classes = makeStyles({
    select: {
      "&:before": {
        borderColor: "white",
      },
      "&:not(.Mui-disabled):hover::before": {
        borderColor: "white",
      },
    },
    icon: {
      fill: "white",
    },
  })();
  return (
    <FormControl variant="standard">
      <Select
        value={props.value}
        style={{ color: "white", fontSize: "15px" }}
        className={classes.select}
        inputProps={{
          classes: {
            icon: classes.icon,
          },
        }}
        onChange={props.onChange}
      >
        {props.children}
      </Select>
    </FormControl>
  );
};

export default CustomSelect;
