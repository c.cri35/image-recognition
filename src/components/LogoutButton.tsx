import { Button, Grid } from "@mui/material";
import React from "react";
import { useAuth } from "../hooks/useAuth";

const LogoutButton = () => {
  const [, , logout] = useAuth();
  return (
    <Grid container justifyContent="flex-end">
      <Button
        onClick={() => {
          logout();
        }}
      >
        Logout
      </Button>
    </Grid>
  );
};

export default LogoutButton;
