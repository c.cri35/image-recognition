import { useState, useEffect, Dispatch, SetStateAction } from "react";

const getStorageValue = (key: string, defaultValue: string) => {
  const saved = localStorage.getItem(key);
  return saved || defaultValue;
};

const useLocalStorage = (
  key: string,
  defaultValue: string
): [string, Dispatch<SetStateAction<string>>] => {
  const [value, setValue] = useState(() => {
    return getStorageValue(key, defaultValue);
  });

  useEffect(() => {
    localStorage.setItem(key, value);
  }, [key, value]);

  return [value, setValue];
};

export default useLocalStorage;
