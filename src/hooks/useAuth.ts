import axios from "../api/instance";
import useLocalStorage from "./useLocalStorage";

export const useAuth = (): [
  string,
  (username: String, password: String) => void,
  () => void
] => {
  const [isLoggedIn, setIsLoggedIn] = useLocalStorage("isLoggedIn", "false");
  const [, setToken] = useLocalStorage("token", "");
  const [, setUserId] = useLocalStorage("userId", "");

  const login = (username: String, password: String) => {
    axios
      .post("login", { email: username, password: password })
      .then((response) => {
        if (response.status === 200) {
          setIsLoggedIn("true");
          setToken(response.data.token);
          setUserId(`${response.data.userId}`);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const logout = () => {
    setIsLoggedIn("false");
    setToken("");
  };

  return [isLoggedIn, login, logout];
};
